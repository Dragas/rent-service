FROM docker.io/library/eclipse-temurin:11.0.20.1_1-jdk-jammy as builder
RUN mkdir /app
WORKDIR /app
COPY . .
RUN chmod +x ./mvnw && ./mvnw install -Dmaven.repo.local=./.m2/repo && mv ./target/rent-service-*.jar ./target/rent-service.jar

FROM docker.io/library/eclipse-temurin:11.0.20.1_1-jdk-jammy as runtime
RUN mkdir /app
WORKDIR /app
COPY --from=builder /app/target/rent-service.jar ./rent-service.jar
CMD ./rent-service.jar