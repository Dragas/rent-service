# Rent Service Implementation

## Building

### Local

In case you have java installed

```bash
chmod +x ./mvnw
./mvnw clean install
./target/rent-service-*.jar
```

Otherwise use docker to build the project

```bash
docker image build . --tag "rent-service:1"
```

### CI

CI Uses `kaniko` to build the images as docker-in-docker does not run on public gitlab runners.
Images are pushed to repository's registry.

## HTTP Interface

Base URI is `/api/v1`

### Types

* `Long` - Signed 64bit integer
* `Integer` - Signed 32bit integer
* `String` - UTF8 byte array
* `BigDecimal` - Arbitrary length and precision floating point number

### Authorization

Each endpoint requires BASIC authorization header with user `a` password `a` as follows

```
Authorization: Basic base64("a:a")
```

where `base64(String)` is a function that converts the argument to its stringified representation of base64
byte array. On failure to authorize the service returns 401 to all requests.

### Errors

In case a given product does not exist, a 404 is returned.

```
404 Not Found
Content-Type: text/plain

No such Product:{id}
```

### Endpoints

#### Product availability

Returns all **rentable** products in the system.

```
GET /products
```

Response

```
200 OK
Content-Type: application/json

{
    id: Long
    title: String
    rentable: Boolean
    initialPrice: BigDecimal
}[]
```

Example request and response

```
GET /products
```

```json
[
    {
        "id": 1,
        "title": "Xbox Series X",
        "rentable": true,
        "initialPrice": 35
    }
]
```

#### Product deal availability

Returns product, and available deals for product with given `id` path parameter

```
GET /products/{id}
```

Response

```200 OK
{
    id: Long
    title: String
    rentable: Boolean
    initialPrice: BigDecimal
    possibleCommitments: {
        id: Long
        commitmentLengthInMonths: Integer
        cost: BigDecimal
    }[]
}
```

Example request and response

```
GET /products/1
```

```json
{
    "id": 1,
    "title": "Xbox Series X",
    "rentable": true,
    "initialPrice": 35,
    "possibleCommitments": [
        {
            "id": 1,
            "commitmentLengthInMonths": null,
            "cost": 35
        },
        {
            "id": 6,
            "commitmentLengthInMonths": 3,
            "cost": 30
        },
        {
            "id": 11,
            "commitmentLengthInMonths": 6,
            "cost": 25
        }
    ]
}
```


#### Price calculator

Returns the cost for how much a given product would be rented. In case one of the deals is selected,
(such as 6 months), the price shall be `Product#initialPrice` + `months` * `Commitment#cost`. Otherwise
a commitment with `null` length shall be selected for the cost.

```
POST /products/{id}/price
Content-Type: application/json

Integer
```

Where `id` is the product to calculate the price for, and body parameter is an integer amounting to
months that user would rent the product.

Response

```
200 OK
Content-Type: application/json

Integer
```

The endpoint responds with scalar value that corresponds to the cost of renting period.

Example request and response:

```
POST /products/1/price
Content-Type: application/json

6
```

```
200 OK
Content-Type: application/json

185
```