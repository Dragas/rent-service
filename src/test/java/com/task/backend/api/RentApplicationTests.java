package com.task.backend.api;

import com.task.backend.api.controller.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Collection;

@SpringBootTest
class RentApplicationTests {

    @Autowired
    private ProductRentService productService;


    @Test
    public void itReturnsOnlyRentableProducts() {
        Collection<Product> result = productService.getAllAvailableProducts();
        for (Product product : result) {
            Assertions.assertEquals(true, product.isRentable(), () -> String.format("Product:%s is not rentable so it must not be in the collection", product.getId()));
        }
    }

    @Test
    public void itReturnsAvailableProductWithPrices() {
        ProductWithPossiblePrices productWithPrices = productService.getProductWithCommitments(1L);
        Assertions.assertNotNull(productWithPrices, "Product:1 must be present in the system and must be rentable");
    }

    @Test
    public void itDoesNotReturnProductWhenIdDoesNotExist() {
        Assertions.assertThrows(RentException.class, () -> {
            ProductWithPossiblePrices productWithPrices = productService.getProductWithCommitments(1111L); // guaranteed to not exist
        }, "Product:1111 must not be returned" );
    }

    @Test
    @Transactional
    // needed since transactions only exist within service's scope, force springboot test runtime to extend it to test's scope.
    // can replace with @datajpatest but then you lose autoconf and others
    public void returnedProductContainsAvailablePrices() {
        ProductWithPossiblePrices productWithPrices = productService.getProductWithCommitments(1L);
        Collection<Price> prices = productWithPrices.getPossibleCommitments();
        Assertions.assertNotNull(prices, "related prices must be available");
        Assertions.assertEquals(3, prices.size(), "there must 3 related prices");
    }

    @Test
    public void oculusQuest2PriceMustMatchFor6MonthCommitmentAfter2Months() {
        BigDecimal result = productService.calculatePrice(5L, 2);
        Assertions.assertEquals(BigDecimal.valueOf(105), result, "oculus quest 2 price must match as per readme");
    }

    @Test
    public void nintendoSwitchPriceMustMatchForNoMonthCommitmentAfter7Months() {
        // FIXME: initial dataset does not contain switch, only switch lite. clarify examples in readme?
        BigDecimal result = productService.calculatePrice(4L, 7);
        Assertions.assertEquals(BigDecimal.valueOf(136), result, "nintendo switch price must match as per readme");
    }

    @Test
    public void xBoxOneXPriceMustMatchFor3MonthCommitmentAfter3Months() {
        // From spec initial + 3 * 3 month price
        BigDecimal expectedResult = BigDecimal.valueOf(35 + 30 * 3);
        BigDecimal result = productService.calculatePrice(1L, 3);
        Assertions.assertEquals(expectedResult, result, "xbox one x price must be 125");
    }

    @Test
    public void SonyPS5PriceMustMatchFor6MonthCommitmentAfter6Months() {
        // From spec initial + 3 * 3 month price
        BigDecimal expectedResult = BigDecimal.valueOf(35 + 25 * 6);
        BigDecimal result = productService.calculatePrice(3L, 6);
        Assertions.assertEquals(expectedResult, result, "PS5 price must be 185");
    }

    @Test
    public void itIsNotPossibleToGetPriceWhenProductDoesNotExist() {
        Assertions.assertThrows(RentException.class, () -> {
            BigDecimal result = productService.calculatePrice(333333L, 1);
        }, "Must throw when there is no such product");
    }
}
