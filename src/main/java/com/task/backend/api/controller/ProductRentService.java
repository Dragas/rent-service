package com.task.backend.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Optional;

@Service
public class ProductRentService {

    private final RentRepository repository;

    @Autowired
    public ProductRentService(RentRepository repository) {
        this.repository = repository;
    }

    public Collection<Product> getAllAvailableProducts() {
        //TODO: Clarify requirement what does it mean "to be available"
        return this.repository.findAllByRentable(true);
    }

    public ProductWithPossiblePrices getProductWithCommitments(Long productId) {
        Optional<Product> possibleProduct = this.repository.findByIdAndRentable(productId, true);
        return possibleProduct.map(ProductWithPossiblePrices::new).orElseThrow(() -> new RentException(String.format("No such Product:%s", productId)));
    }

    public BigDecimal calculatePrice(Long productId, Integer months) {
        Optional<ProductWithPrice> price = repository.getProductWithPrice(productId, months);
        // when commitment is not standard must return default
        if(price.isEmpty()) {
            price = repository.getProductWithDefaultPrice(productId);
        }
        // commitment length overlaps with expected rent months so it can be reused
        if(months == null) {
            months = 1;
        }
        Integer finalMonths = months;
        return price.map((it) -> {
            // initial overlaps with no commitment price, so it might be good idea to ping no commit price instead?
            BigDecimal initial = it.getProduct().getInitialPrice();
            BigDecimal commitmentPrice = it.getPrice().getCost();
            return initial.add(commitmentPrice.multiply(BigDecimal.valueOf(finalMonths)));
        })
                // only way this can fail if product does not exist
                .orElseThrow(() -> new RentException(String.format("No such Product:%s", productId)));
    }
}
