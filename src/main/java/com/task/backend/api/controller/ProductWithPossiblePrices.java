package com.task.backend.api.controller;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import java.util.Collection;

public class ProductWithPossiblePrices {
    private final Product product;

    public ProductWithPossiblePrices(Product product) {
        this.product = product;
    }

    @JsonUnwrapped
    public Product getProduct() {
        return product;
    }

    public Collection<Price> getPossibleCommitments() {
        return product.getCommitments();
    }
}
