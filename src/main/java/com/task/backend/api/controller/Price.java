package com.task.backend.api.controller;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "price")
public class Price {

    @Id
    @Column(name = "id", unique = true)
    private Long id;
    @Column(name = "commitment_months")
    private Integer commitmentLengthInMonths;
    @Column(name = "value")
    private BigDecimal cost;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCommitmentLengthInMonths() {
        return commitmentLengthInMonths;
    }

    public void setCommitmentLengthInMonths(Integer commitmentLengthInMonths) {
        this.commitmentLengthInMonths = commitmentLengthInMonths;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }
}
