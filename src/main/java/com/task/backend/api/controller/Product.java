package com.task.backend.api.controller;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @Column(name = "id", unique = true)
    private Long id = -1L;
    @Column(name = "title", unique = true)
    private String title = "";
    @Column(name = "rentable")
    private Boolean rentable = false;
    @Column(name = "initial_price")
    private BigDecimal initialPrice = BigDecimal.ZERO;

    @OneToMany
    @JoinTable(
            name = "product_price",
            joinColumns = {
                    @JoinColumn( name = "product_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn( name = "price_id", referencedColumnName = "id")
            }
    )
    private Collection<Price> commitments;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean isRentable() {
        return rentable;
    }

    public void setRentable(Boolean rentable) {
        this.rentable = rentable;
    }

    @JsonIgnore
    public Collection<Price> getCommitments() {
        return commitments;
    }

    public void setCommitments(Collection<Price> commitments) {
        this.commitments = commitments;
    }

    public BigDecimal getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(BigDecimal initialPrice) {
        this.initialPrice = initialPrice;
    }
}
