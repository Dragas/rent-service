package com.task.backend.api.controller;


import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.Optional;

public interface RentRepository extends JpaRepository<Product, Long> {

    Optional<Product> findByIdAndRentable(Long id, Boolean rentable);

    Collection<Product> findAllByRentable(Boolean rentable);

    @Query(value = "select pwp from ProductWithPrice pwp join pwp.product product join pwp.price price where product.id = :productId and price.commitmentLengthInMonths = :commitmentLength")
    Optional<ProductWithPrice> getProductWithPrice(Long productId, Integer commitmentLength);

    @Query(value = "select pwp from ProductWithPrice pwp join pwp.product product join pwp.price price where product.id = :productId and price.commitmentLengthInMonths is null")
    Optional<ProductWithPrice> getProductWithDefaultPrice(Long productId);
}
