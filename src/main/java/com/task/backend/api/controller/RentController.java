package com.task.backend.api.controller;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Collection;

@RestController
@RequestMapping("/api/v1")
public class RentController {

    private final ProductRentService service;

    public RentController(ProductRentService service) {
        this.service = service;
    }


    @RequestMapping(
            method = RequestMethod.GET,
            path = "/products"
    )
    @Cacheable(cacheNames = "default")
    public Collection<Product> getAllAvailableProducts() {
        return service.getAllAvailableProducts();
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "/products/{id}"
    )
    @Cacheable(cacheNames = "default")
    public ProductWithPossiblePrices getAvailablePricesForProduct(@PathVariable(name = "id") Long productId) {
        return service.getProductWithCommitments(productId);
    }

    @RequestMapping(
            method = RequestMethod.POST,
            path = "/products/{id}/price",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Cacheable(cacheNames = "default")
    public BigDecimal getExpectedRentCostForProduct(@PathVariable(name = "id") Long productId, @RequestBody Integer months) {
        return service.calculatePrice(productId, months);
    }
}
