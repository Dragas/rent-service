package com.task.backend.api.controller;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "product_price")
public class ProductWithPrice implements Serializable {

    @ManyToOne
    @Id
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;
    @ManyToOne
    @Id
    @JoinColumn(name = "price_id", referencedColumnName = "id")
    private Price price;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }
}