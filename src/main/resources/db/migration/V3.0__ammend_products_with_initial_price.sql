
alter table product add column initial_price decimal not null default 0;
-- title are unique so its okay to refer to them directly
update product set initial_price = 35 where title = 'Xbox Series X';
update product set initial_price = 25 where title = 'Xbox Series S';
update product set initial_price = 35 where title = 'Sony PS5';
update product set initial_price = 17 where title = 'Nintendo Switch Lite';
update product set initial_price = 35 where title = 'Oculus Quest 2';
update product set initial_price = 30 where title = 'Oculus Quest';
update product set initial_price = 35 where title = 'Oculus Rift S';
update product set initial_price = 45 where title = 'HTC Vive Cosmos';
update product set initial_price = 35 where title = 'iPhone X';
update product set initial_price = 45 where title = 'Samsung 20';
